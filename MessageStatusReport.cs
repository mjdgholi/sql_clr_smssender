﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQL_CLR_SMSSender
{
    public class MessageStatusReport
    {
        public string ClientId { get; set; }

        public string SmsStatus { get; set; }
    }
}