﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using System.Net;
using SQL_CLR_SMSSender.com.adpdigital.ws;
using SQL_CLR_SMSSender.com.magfa.sms;

namespace SQL_CLR_SMSSender
{
    public class SmsManager
    {
        public static string MagfaUserName { get; set; }
        public static string MagfaPassword { get; set; }
        public static string AdpUserName { get; set; }
        public static string AdpPassword { get; set; }
        //senderNumbers = "9820000125";
        // AdpUserName = "araman";
        //AdpPassword = "123654789";
        public SmsManager()
        {
            MagfaUserName = "";
            MagfaPassword = "";
            AdpUserName = "dokhaniyat";
            AdpPassword = "123456987";
        }

        [Microsoft.SqlServer.Server.SqlProcedure]
        public static string SendSms(int smsServiceProvider, string messageBody, string recipientNumbers,
            string senderNumbers, int messageClasses, string checkingMessageIds)
        {
            if (smsServiceProvider == 1) // سرویس مگفا
            {
                using (var sms = new SoapSmsQueuableImplementationService())
                {
                    const string domain = "magfa";
                    var senderNumbersArr = senderNumbers.Split(',');
                    var messageBodyArr = messageBody.Split(',');
                    var recipientNumbersArr = recipientNumbers.Split(',');
                    var messageClassesArr = new[] {messageClasses};
                    long[] checkingMessageIdsArr = checkingMessageIds.Split(',').Select(n => long.Parse(n)).ToArray();
                    var encodings = new[] {0}; // encodeing automatic
                    var priority = new[] {1}; // اولویت معمولی
                    string[] udh = {};
                    sms.Credentials = new NetworkCredential(MagfaUserName, MagfaPassword);
                    var res = sms.enqueue(domain, messageBodyArr, recipientNumbersArr, senderNumbersArr, encodings, udh,
                        messageClassesArr, priority, checkingMessageIdsArr);
                    //  Label1.Text = res[0].ToString();
                    return "Success";
                }
            }
            else if (smsServiceProvider == 2) // سرویس آتیه داده پرداز
            {
                using (var sms = new com.adpdigital.ws.JaxRpcMessagingServiceService())
                {
                    senderNumbers = "98200005126";
                    var recipientNumbersArr = recipientNumbers.Split(',');
                    var checkingMessageIdsArr = checkingMessageIds.Split(',');
                    const short encodings = 2; // یک : فقط لاتین    دو: یونیکد                    
                    const short messageType = 1; // PLAIN TEXT
                    const bool longSupported = true;
                    sms.Credentials = new NetworkCredential("dokhaniyat", "123456987");
                    recipientNumbersArr = CheckMobileNumber(recipientNumbersArr);
                    //------------
                    var partSize = 250;
                    for (var i = 0; i < (float) recipientNumbersArr.Length/partSize; i++)
                    {
                        var recipientNumberPart = recipientNumbersArr.Skip(i*partSize).Take(partSize).ToArray();
                        var checkingMessageIdsArrPart = checkingMessageIdsArr.Skip(i*partSize).Take(partSize).ToArray();
                        sms.send("dokhaniyat", "123456987", senderNumbers, recipientNumberPart, "", "",
                            checkingMessageIdsArrPart, messageType, encodings, longSupported, DateTime.Now, messageBody);
                    }
                    //------------                   
                    return "Success";
                }
            }
            return "Error! SmsServiceProvide Not Set";
        }

        private static string[] CheckMobileNumber(string[] mobiles)
        {
            string[] correctedMobile = mobiles;
            for (int i = 0; i < mobiles.Length; i++)
            {
                if (mobiles[i].StartsWith("0"))
                {
                    correctedMobile[i] = "98" + mobiles[i].Substring(1, mobiles[i].Length - 1);
                }
            }
            return correctedMobile;
        }

        [Microsoft.SqlServer.Server.SqlFunction]
        public static long GetMessageId(long chkMessageId)
        {
            using (var sms = new SoapSmsQueuableImplementationService())
            {
                const string domain = "magfa";
                sms.Credentials = new NetworkCredential(MagfaUserName, MagfaPassword);
                return sms.getMessageId("magfa", chkMessageId);
            }
        }

        [Microsoft.SqlServer.Server.SqlFunction]
        public static int GetRealMessageStatus(long messageId)
        {
            using (var sms = new SoapSmsQueuableImplementationService())
            {
                long[] messageIdStrArr = new[] {messageId};
                const string domain = "magfa";
                sms.Credentials = new NetworkCredential(MagfaUserName, MagfaPassword);
                return sms.getRealMessageStatuses(messageIdStrArr)[0];
            }
        }

        [Microsoft.SqlServer.Server.SqlFunction(FillRowMethodName = "FillRow_MessageStatusGeneral")]
        public static IEnumerable GetMessageStatusGeneral(int smsServiceProvider, string checkingMessageIds)
        {


            if (smsServiceProvider == 1) // سرویس مگفا
            {
                long[] checkingMessageIdsArr = checkingMessageIds.Split(',').Select(s => long.Parse(s)).ToArray();
                using (var sms = new SoapSmsQueuableImplementationService())
                {
                    ArrayList clientIdStr = new ArrayList();
                    sms.Credentials = new NetworkCredential(MagfaUserName, MagfaPassword);
                    for (int i = 0; i < checkingMessageIdsArr.Length; i++)
                    {
                        clientIdStr.Add(sms.getMessageId("magfa", checkingMessageIds[0]));
                    }
                    long[] clientIdArr = (long[]) clientIdStr.ToArray(typeof (long));
                    // return sms.getRealMessageStatuses(clientIdArr)[0]; ////////////// Check
                }
            }
            if (smsServiceProvider == 2) // سرویس آتیه داده پرداز
            {
                using (var sms = new com.adpdigital.ws.JaxRpcMessagingServiceService())
                {
                    string[] clientIdArr = checkingMessageIds.Split(',');
                    var result = sms.statusReport("dokhaniyat", "123456987", "98200005126", "clientId", null, clientIdArr);
                    var retValue = (from res in result.report
                        select (new MessageStatusReport {ClientId = res.clientId, SmsStatus = res.status.ToString()}))
                        .ToList();
                    return retValue;
                }
            }
            return null; // error
        }

        public static void FillRow_MessageStatusGeneral(Object obj, out string clientId, out string smsStatus)
        {
            var statusReportList = (MessageStatusReport) obj;

            clientId = statusReportList.ClientId;
            smsStatus = statusReportList.SmsStatus;
        }

        [Microsoft.SqlServer.Server.SqlFunction]
        public static long GetBalanceOfSmsServiceProvider(int smsServiceProvider, string balanceType)
        {
            if (smsServiceProvider == 1) // سرویس مگفا
            {
                using (var sms = new SoapSmsQueuableImplementationService())
                {
                    sms.Credentials = new NetworkCredential(MagfaUserName, MagfaPassword);
                    return long.Parse(sms.getCredit("magfa").ToString());

                }
            }
            if (smsServiceProvider == 2) // سرویس آتیه داده پرداز
            {
                using (var sms = new com.adpdigital.ws.JaxRpcMessagingServiceService())
                {
                    short facilityId = -1;
                    if ((balanceType.ToLower().Trim() == "recieve"))
                    {
                        facilityId = 1;
                    }
                    else if ((balanceType.ToLower().Trim() == "send"))
                    {
                        facilityId = 2;
                    }
                    BalanceResult balanceResult = sms.getBalance("dokhaniyat", "123456987", facilityId);
                    if (balanceResult.status == 0)
                    {
                        return balanceResult.balance;
                    }
                }
            }
            return -1000; // Error            
        }
    }
}