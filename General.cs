﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SQL_CLR_SMSSender
{
    public class General
    {
        public string ConnectionString { get; set; }

        public General(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public int GetDefaultSmsServiceProvider()
        {
            var sqlCommand = new SqlCommand();
            sqlCommand.Connection = new SqlConnection(this.ConnectionString);
            sqlCommand.Connection.Open();
            sqlCommand.CommandType = CommandType.Text;
            sqlCommand.CommandText = " select * from SmsPanel.SmsServiceProvider where IsDefault = 1 ";
            var reader = sqlCommand.ExecuteReader();
            if (reader.Read())
            {
                return Int32.Parse(reader["SmsServiceProviderId"].ToString());
            }
            return -1;
        }
    }
}